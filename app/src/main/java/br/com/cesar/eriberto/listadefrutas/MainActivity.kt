package br.com.cesar.eriberto.listadefrutas

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import br.com.cesar.eriberto.listadefrutas.WebClient.FrutaWebClient
import br.com.cesar.eriberto.listadefrutas.model.Fruta
import br.com.cesar.eriberto.listadefrutas.retrofit.CallbackResponse
import br.com.cesar.eriberto.listadefrutas.utils.MinhaAplicacao
import br.com.cesar.eriberto.listadefrutas.utils.Utils

class MainActivity : AppCompatActivity() {

    val frutaDao = MinhaAplicacao.database?.frutaDao()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        baixarListaDeFrutas()

        val listaSalva: List<Fruta> = frutaDao?.all() as ArrayList<Fruta>

        listaSalva.forEach {
            Log.i("Show", it.nome + ", OK man")
        }
    }

    fun baixarListaDeFrutas(){
        if(Utils.isConnected(this)){

            FrutaWebClient().list(object : CallbackResponse<List<Fruta>> {
                override fun success(response: List<Fruta>) {

                    response.forEach {
                        Log.i("Fruta",it.nome)

                        frutaDao?.add(it)
                    }

                }

                override fun failure(response: String?) {
                    var dialog = AlertDialog.Builder(this@MainActivity)

                    dialog.setTitle(response)
                    dialog.setNeutralButton("Ok") { dialog, which ->

                        dialog.dismiss()
                    }
                    dialog.show()
                }

            })

        }else{

        }


    }
}
