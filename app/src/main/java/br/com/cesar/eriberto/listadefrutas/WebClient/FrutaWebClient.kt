package br.com.cesar.eriberto.listadefrutas.WebClient

import android.util.Log
import br.com.cesar.eriberto.listadefrutas.model.Fruta
import br.com.cesar.eriberto.listadefrutas.retrofit.CallbackResponse
import br.com.cesar.eriberto.listadefrutas.retrofit.RetrofitInitializer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FrutaWebClient {

    fun list(callbackResponse: CallbackResponse<List<Fruta>>) {
        val call = RetrofitInitializer().frutaService().list()
        call.enqueue(object : Callback<List<Fruta>> {

            override fun onResponse(call: Call<List<Fruta>>?, response: Response<List<Fruta>>?) {
                response?.body()?.let {
                    val fritas: List<Fruta> = it

                    Log.i("codigo", call?.hashCode().toString())
                    callbackResponse.success(fritas)
                }
            }

            override fun onFailure(call: Call<List<Fruta>>?, t: Throwable?) {
                Log.e(">>>>>", t?.localizedMessage)
                Log.e(">>>>>", t?.message)
                callbackResponse.failure(t?.message.toString())
                //Toast.makeText(applicationContext, t?.message, Toast.LENGTH_SHORT).show()
            }
        })
    }
}