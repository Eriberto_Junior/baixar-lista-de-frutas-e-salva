package br.com.cesar.eriberto.listadefrutas.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import br.com.cesar.eriberto.listadefrutas.model.Fruta

@Dao
interface FrutaDAO {

    @Query("Select * FROM Fruta")
    fun all(): List<Fruta>

    @Insert
    fun add(vararg fruta: Fruta)

    @Delete
    fun delete(vararg fruta: Fruta)
}