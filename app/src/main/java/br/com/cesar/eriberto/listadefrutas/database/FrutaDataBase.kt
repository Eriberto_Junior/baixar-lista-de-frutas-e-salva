package br.com.cesar.eriberto.listadefrutas.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import br.com.cesar.eriberto.listadefrutas.dao.FrutaDAO
import br.com.cesar.eriberto.listadefrutas.model.Fruta

@Database(entities = arrayOf(Fruta::class), version = 1)
abstract class FrutaDataBase : RoomDatabase() {

    abstract fun frutaDao(): FrutaDAO

}