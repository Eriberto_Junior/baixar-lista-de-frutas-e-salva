package br.com.cesar.eriberto.listadefrutas.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
class Fruta (nome: String, descricao: String, preco:Int) {

    @PrimaryKey(autoGenerate = true)
    var id:Int = 0

    @ColumnInfo
    var nome: String = nome

    @ColumnInfo
    var descricao: String = descricao

    @ColumnInfo
    var preco:Int = preco

}