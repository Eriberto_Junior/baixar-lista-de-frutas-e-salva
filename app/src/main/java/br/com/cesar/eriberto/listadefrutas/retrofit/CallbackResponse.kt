package br.com.cesar.eriberto.listadefrutas.retrofit

interface CallbackResponse<T> {

    fun success(response: T)

    fun failure(response: String?)
}