package br.com.cesar.eriberto.listadefrutas.retrofit

import br.com.cesar.eriberto.listadefrutas.service.FrutasService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInitializer {

    val retrofit = Retrofit.Builder()
            .baseUrl("https://api.myjson.com/bins/10g3d4/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()


    fun frutaService() = retrofit.create(FrutasService::class.java)
}