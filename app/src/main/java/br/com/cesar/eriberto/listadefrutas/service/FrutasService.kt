package br.com.cesar.eriberto.listadefrutas.service

import br.com.cesar.eriberto.listadefrutas.model.Fruta
import retrofit2.Call
import retrofit2.http.GET

interface FrutasService {

    @GET(".")
    fun list(): Call<List<Fruta>>
}