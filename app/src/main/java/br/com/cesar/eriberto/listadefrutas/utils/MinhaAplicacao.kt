package br.com.cesar.eriberto.listadefrutas.utils

import android.app.Application
import android.arch.persistence.room.Room
import br.com.cesar.eriberto.listadefrutas.database.FrutaDataBase

class MinhaAplicacao : Application() {

    companion object {
        var database: FrutaDataBase? = null
    }

    override fun onCreate() {
        super.onCreate()
        MinhaAplicacao.database =  Room.databaseBuilder(this, FrutaDataBase::class.java, "meu_banco-db").allowMainThreadQueries().build()
    }
}